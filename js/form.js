
// List of Objects ( variables) for the code
//make sure to just select the element you are looking for
let ourForm = document.getElementById('ourForm');
// handle the input feild
let ourField = document.getElementById('ourField');
// handels the list
let theList = document.getElementById('theList');

// listens to the form
ourForm.addEventListener('submit',(e)=> {
    e.preventDefault();
    createItem(ourField.value);
});

function createItem(x) {
    // html var
    let ourHTML = `<li>${x} <button onclick="deleteItem(this)">Delete</button></li>`;
    // the list object
    theList.insertAdjacentHTML('beforeend',ourHTML);
    // clear the feild once done
    ourField.value = "";
    // re-select the feild to keep adding list
    ourField.focus();

    // testing code
    console.log(x);
}

function deleteItem(elementToDelete) {
    elementToDelete.parentElement.remove();

    // testing item
    console.log(elementToDelete)
}