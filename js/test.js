


// What is the Purpose of this App
    // Creating a request response app and how to set that up
    //This is the core of all applcations
// What is the ingredient
    // missing ingredient is a database, Going to start by working on MongoDB
    // linking databases are where all your data is being stored
// What are we going to build towards
    // BUilding toward fullstack js applications
    // Building 2 apps to help develop




// setting up full scale express server
    // express variable
    let express = require('express');
    // object for our app
    let ourApp = express();
    //enables the user.body object to have a value submited to express, boiler plate code
    ourApp.use(express.urlencoded({extended: false}));
    // set up requests for the express server, UI for the user
    ourApp.get('/', function (req, res) {
        res.send(`
            <form action="/answer" method="POST">
            <p> What color is the sky on a clear day?</p>
            <input name="skyColor" autocomplete="false">
            <button>Submit</button>
            </form>
        `)
    });

    // post request
    // post request is when you enter a bit of data to be submitted to a server
    ourApp.post('/answer',function (req, res) {
       if(req.body.skyColor.toUpperCase() == "BLUE"){
           res.send(`
            <p>Congrats you are correct</p>
            <a href="/">Back to Home Page</a>
           `)
       } else{
           res.send(`
            <p>What the Hell are You Smoking??</p>
            <a href="/">Back to Home Page</a>
           `)
       }
    });

    // get request
    // get requests are when you type in the url a bit of data you are looking for
    ourApp.get('/answer',function (req, res) {
        res.send('You wont see the data here fool');
    });
    // Set the port
    ourApp.listen(3000);














// Create a server that can listen to incoming requests
// visit the server in the browser to see the values that we are reaching out to

// set up http variable
// let http = require('http');
// // create node environment structure
//     // create object for the server
//     // req= request res = response
//     let ourApp = http.createServer(function (req,res) {
//         // base url
//         if(req.url == '/') {
//             res.end('You made the server work');
//         }
//         // about page url
//         if(req.url =='/about') {
//             res.end('Now your looking at my bio');
//         }
//         res.end('We Can not find the page your looking for, come back later');
//     });
//     // set the server to listen to specific port
//     ourApp.listen(3000);
