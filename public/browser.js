// Testing Feature options
    // let userInput = prompt('Enter you need text');
    // console.log(userInput);
    // example of a promise, way to test an action and see how long it takes to send some data
    //axios.post('/update-item',{text: userInput}).then().catch();
    // way to test that a new item is being added to the database with out constantly sending data
        // testing feature
        //e.preventDefault();
        // axios command to node server for deleting the item
        //axios.post('/create-item',{ text: createField.value}).then(function() {
        // do the updates on the user interface
        // this line then deletes the selected item
        // need to add create html for new item
        //  alert("You just added an item");
        //
        // }).catch(function () {
        // console.log("Something broke here,also can be some type of object")
        // });


// html template for user input
function itemTemplate(item){
    return `<li class="list-group-item list-group-item-action d-flex align-items-center justify-content-between">
              <span class="item-text">${item.text}</span>
                <!-- first level parent element-->
              <div>
                <!--example of a template literal, make sure to include the undscore line. -->
              <button data-id="${item._id}" class="edit-me btn btn-secondary btn-sm mr-1">Edit</button>
              <button data-id="${item._id}" class="delete-me btn btn-danger btn-sm">Delete</button>
              </div>
              </li>`
};

// initial page load render
    let ourHTML = items.map(function (item) {
        return itemTemplate(item)
    }).join('')

    document.getElementById('item-list').insertAdjacentHTML("beforeend", ourHTML)



// create feature for user side

//variable for accessing the create-feild id
let createField = document.getElementById('create-field');
    // first argument is for when user submits handles both button select or return button is hit 
document.getElementById('create-form').addEventListener("submit", function (e) {
    // testing feature
    e.preventDefault();
    // axios command to node server for deleting the item
    axios.post('/create-item',{ text: createField.value}).then(function(response) {
        // do the updates on the user interface
        // this line then deletes the selected item
       // need to add create html for new item
        document.getElementById("item-list").insertAdjacentHTML("beforeend", itemTemplate(response.data));
        createField.value = "";
        createField.focus();
        //
    }).catch(function () {
        console.log("Something broke here,also can be some type of object")
    });
});

document.addEventListener('click',function (e) {
    // delete feature
    if(e.target.classList.contains('delete-me')) { // event looking for a target in a classlist that contains the element edit me
        // prompots user to make sure they want to delete the feature
        if(confirm("Are you Sure you want to Delete?")) {
            // axios command to node server for deleting the item
            axios.post('/delete-item',{ id: e.target.getAttribute("data-id")}).then(function() {
                // do the updates on the user interface
                // this line then deletes the selected item
                e.target.parentElement.parentElement.remove();
                //
            }).catch(function () {
                console.log("Somthing broke here,also can be some type of object")
            });

        }
    };



    // Update Feature of the Application
    if(e.target.classList.contains('edit-me')){ // event looking for a target in a classlist that contains the element edit me
        //
        // second argument is going to pre populated the selected item that the user picked
      let userInput = prompt('Enter you need text', e.target.parentElement.parentElement.querySelector(".item-text").innerHTML);
          // first argument is the url that you are sending info to
            // second argument is a javascript object, it is the data that is being sent
            // example of a promise, way to test an action and see how long it takes to send some data
            // catch is where you can check if something breaks along the way
        if(userInput){
            axios.post('/update-item',{text: userInput, id: e.target.getAttribute("data-id")}).then(function() {
                // do the updates on the user interface
                // this line is looking at which item was selected by the user on the UI
                e.target.parentElement.parentElement.querySelector(".item-text").innerHTML =userInput;
                //
            }).catch(function () {
                console.log("Somthing broke here,also can be some type of object")
            });
        }

    }
});



