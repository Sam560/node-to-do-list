// Helpful code snippets for testing purposees
    // way to test weather  of data can be logged
        //console.log(req.body.item);




// express framework for setting up our server
let express = require('express')
let mongodb = require('mongodb')

let app = express()
let db

let sanitizeHTML = require('sanitize-html')

app.use(express.static('public'))


let connectionString = 'mongodb+srv://0@cluster0-9gl3z.mongodb.net/todo-app?retryWrites=true&w=majority'
mongodb.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    db = client.db()
    app.listen(3000)
})

// a synchronization from browser to server
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//function for handling password authentication
function passwordProtected(req, res, next){
    res.set("WWW-Authenticate", 'Basic realm="Simple To-Do App')
    console.log(req.headers.authorization)
    if(req.headers.authorization == "Basic U2FtOjEyMzQ="){
        next()
    } else {
        res.status(401).send("Authentication Required ")
        console.log("intruder detected ")
    }
}

// way to assign a function across all routs
app.use(passwordProtected)

// sends back response
app.get('/', function(req, res) {
    db.collection('items').find().toArray(function (err, items) {
        res.send(`<!DOCTYPE html>
  <html>
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Simple To-Do App</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
  <h1 class="display-4 text-center py-1">To-Do App!</h1>
  
  <div class="jumbotron p-3 shadow-sm">
  <form id="create-form" action="/create-item" method="POST">
  <div class="d-flex align-items-center">
  <input id="create-field" name="item" autofocus autocomplete="off" class="form-control mr-3" type="text" style="flex: 1;">
  <button class="btn btn-primary">Add New Item</button>
  </div>
  </form>
  </div>
  
  <ul id="item-list" class="list-group pb-5">
   
  </ul>
  
  </div>
            <!--axios cdn library-->
        <script>
            let items = ${JSON.stringify(items)};
        </script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
       <script src="/browser.js"></script>
        </body>
  </html>`)
    }); // find is a method for searching databases
    // sends back response

});

// below are three examples of common crud methods for dealing with adding, updating, or removing data.


// this function creates new items for the list
app.post('/create-item', function(req, res) {
    // santizie your html data prior to uploading data
    let safeText = sanitizeHTML(req.body.text,{allowedTags: [], allowedAttributes: {} })
    db.collection('items').insertOne({text: safeText}, function(err, info) {
        res.json(info.ops[0])
    })
});

// place where you can communicate with the database, express server
app.post('/update-item',function (req, res) {
    // logging data to the console
    // findOne method will update a specific item
    // First argument, tells the server which item was selected to be edited
    // second argument, tells to look at what the user chose to edit
    // third argument is the function call once the database has processed it
    let safeText = sanitizeHTML(req.body.text,{allowedTags: [], allowedAttributes: {} })
    db.collection('items').findOneAndUpdate({_id: new mongodb.ObjectId(req.body.id)},{$set: {text: safeText}},function () {
        // sending response to the server
        res.send('Success')
    });

});

// post request to node server to delete the selected item
app.post('/delete-item', function (req, res) {
    //deleteOne is curd method for deleteing selected item
    db.collection('items').deleteOne( {_id: new mongodb.ObjectId(req.body.id)}, function () {
        res.send('Success, your itme was deleted')
    })
})

//user name and password for to-dolist table




// way to loop through list of items example
// ${items.map(function (item) {
//                    // li is the over all parent element of items below
//            return `<li class="list-group-item list-group-item-action d-flex align-items-center justify-content-between">
//              <span class="item-text">${item.text}</span>
// <!-- first level parent element-->
// <div>
// <!--example of a template literal, make sure to include the undscore line. -->
// <button data-id="${item._id}" class="edit-me btn btn-secondary btn-sm mr-1">Edit</button>
//     <button data-id="${item._id}" class="delete-me btn btn-danger btn-sm">Delete</button>
//     </div>
//     </li>`
// }).join('')}